package com.wavehq.challenge.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

/**
 * The type Payroll file data. This file isn't persisted in the database.
 * It's only used to store the processed version data of the file, and is used to insert working hours the data into the database.
 *
 */
@Builder
@Getter
@AllArgsConstructor
public class PayrollFileData {
    private String fileName;
    private List<String> records;
}
