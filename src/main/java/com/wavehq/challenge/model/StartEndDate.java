package com.wavehq.challenge.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * This class isn't persisted in the database.
 * This class is used to represent the start and end date of a period. It's used when the user wants to generate reports.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class StartEndDate {

    private final Long startDate;
    private final Long endDate;

}
