package com.wavehq.challenge.model;

import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Objects;
import java.util.UUID;

import static java.time.Instant.now;

/**
 * The type Payroll file. This file is persisted in the database in order to keep track of what files are uploaded and throw exceptions if something duplicated uploads.
 *
 * This file saves the payroll file data. File id is related for period, and file path is the path for file when it is uploaded.
 * We also save uploadedAt which is the time when the file was uploaded.
 *
 */
@Builder
@Getter
@Setter
@ToString
@NoArgsConstructor(force = true)
@AllArgsConstructor
@RequiredArgsConstructor
@Entity
public class PayrollFile {

    @Id
    @GeneratedValue(generator = "UUID")
    @Column(columnDefinition = "varchar(36)")
    @Type(type = "uuid-char")
    private UUID id;

    private final String fileId;
    private final String filePath;

    private Long uploadedAt;

    @PrePersist
    public void prePersist() {
        uploadedAt = now().toEpochMilli();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        PayrollFile that = (PayrollFile) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
