package com.wavehq.challenge.model;

import lombok.*;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.UUID;

import static java.util.Optional.of;

/**
 * The type Working hours history. This class is persisted in the database.
 * This class represents the history of the working hours of an employee, and is used to calculate the total working hours of an employee, and their payroll.
 *
 * EmployeeId is the id of the employee.
 * Date is the date of the working hours.
 * WorkingHours is the number of hours worked.
 * JobGroup is the job group of the employee.
 * GetAmountPaid returns the amount paid to the employee.
 */
@Builder
@Getter
@Setter
@ToString
@NoArgsConstructor(force = true)
@AllArgsConstructor
@RequiredArgsConstructor
@Entity
public class WorkingHoursHistory {

    @Id
    @GeneratedValue(generator = "UUID")
    @Column(columnDefinition = "varchar(36)")
    @Type(type = "uuid-char")
    private UUID id;

    @Column(nullable = false)
    private final String employeeId;

    @Column(nullable = false)
    private final Long date;

    @Column(nullable = false)
    private final Double workingHours;

    @Column(nullable = false)
    private final String jobGroup;

    public double getAmountPaid() {
        assert jobGroup != null;
        assert workingHours != null;

        return of(jobGroup).filter(x -> x.equalsIgnoreCase("A"))
                .map(x -> workingHours * 20)
                .orElseGet(() -> workingHours * 30);

    }


    public record EmployeeIdJobGroup(String employeeId, String jobGroup) { }

    public EmployeeIdJobGroup getEmployeeIdJobGroup() {
        return new EmployeeIdJobGroup(employeeId, jobGroup);
    }


}
