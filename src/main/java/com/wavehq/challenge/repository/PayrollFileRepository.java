package com.wavehq.challenge.repository;

import com.wavehq.challenge.model.PayrollFile;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface PayrollFileRepository extends JpaRepository<PayrollFile, String> {

    Optional<PayrollFile> findByFileId(String s);

}
