package com.wavehq.challenge.repository.specification;

import com.wavehq.challenge.model.WorkingHoursHistory;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public record WorkingHistoryBetween(Long start, Long end) implements Specification<WorkingHoursHistory> {

    @Override
    public Predicate toPredicate(Root<WorkingHoursHistory> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        return cb.between(root.get("date"), start, end);
    }

}
