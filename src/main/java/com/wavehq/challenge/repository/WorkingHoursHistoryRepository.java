package com.wavehq.challenge.repository;

import com.wavehq.challenge.model.WorkingHoursHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.UUID;

public interface WorkingHoursHistoryRepository extends
        JpaRepository<WorkingHoursHistory, UUID>, JpaSpecificationExecutor<WorkingHoursHistory> {

    WorkingHoursHistory findFirstByOrderByDateAsc();
    WorkingHoursHistory findFirstByOrderByDateDesc();

}
