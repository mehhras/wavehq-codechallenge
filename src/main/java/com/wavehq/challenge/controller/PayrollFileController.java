package com.wavehq.challenge.controller;

import com.wavehq.challenge.service.PayrollFileService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import static org.springframework.http.ResponseEntity.ok;


@Slf4j
@RestController
@RequestMapping("wavehq/challenge")
@RequiredArgsConstructor
public class PayrollFileController {

    private final PayrollFileService payrollFileService;

    @PostMapping("file/upload")
    public ResponseEntity<Void> upload(@RequestParam("file") MultipartFile file) {
        log.info("Uploading file {}", file.getOriginalFilename());
        payrollFileService.importFile(file);
        log.info("File {} uploaded", file.getOriginalFilename());
        return ok().build();
    }

}
