package com.wavehq.challenge.controller;


import com.wavehq.challenge.dto.PayrollReport;
import com.wavehq.challenge.service.WorkingHoursHistoryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.ResponseEntity.ok;

@Slf4j
@RestController
@RequestMapping("wavehq/challenge")
@RequiredArgsConstructor
public class PayrollReportController {

    private final WorkingHoursHistoryService workingHoursHistoryService;

    @GetMapping("report")
    public ResponseEntity<PayrollReport> report() {
        log.info("Generating payroll report");
        return ok(workingHoursHistoryService.generateReport());

    }

}
