package com.wavehq.challenge.utils;

import com.wavehq.challenge.model.StartEndDate;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Optional;

import static java.time.Instant.ofEpochMilli;
import static java.time.LocalDate.ofInstant;
import static java.time.LocalDate.parse;
import static java.time.ZoneId.of;
import static java.time.format.DateTimeFormatter.ofPattern;

@Component
public class DateUtils {

    public static final String INPUT_DATE_FORMAT = "d/M/yyyy";
    public static final String OUTPUT_DATE_FORMAT = "yyyy-MM-dd";
    private final DateTimeFormatter inputFormatter = ofPattern(INPUT_DATE_FORMAT);
    private final DateTimeFormatter outputFormatter = ofPattern(OUTPUT_DATE_FORMAT);

    public Long toEpoch(String date) {
        return parse(
                date,
                inputFormatter
        ).atStartOfDay().atZone(of("UTC")).toInstant().toEpochMilli();

    }

    public StartEndDate getStartAndEndDate(Long date) {
        LocalDate localDate = ofInstant(
                ofEpochMilli(date),
                of("UTC")
        );
        return Optional.of(localDate).filter(d -> d.getDayOfMonth() < 16)
                .map(d -> StartEndDate.builder()
                        .startDate(d.withDayOfMonth(1).atStartOfDay().atZone(of("UTC")).toInstant().toEpochMilli())
                        .endDate(d.withDayOfMonth(15).atStartOfDay().atZone(of("UTC")).toInstant().toEpochMilli())
                        .build())
                .orElseGet(() -> StartEndDate.builder()
                        .startDate(localDate.withDayOfMonth(16).atStartOfDay().atZone(of("UTC")).toInstant().toEpochMilli())
                        .endDate(localDate.withDayOfMonth(localDate.lengthOfMonth()).atStartOfDay().atZone(of("UTC")).toInstant().toEpochMilli())
                        .build());
    }

    public long getNextDay(Long date) {
        LocalDate localDate = ofInstant(
                ofEpochMilli(date),
                of("UTC")
        );
        return localDate.plusDays(1).atStartOfDay().atZone(of("UTC")).toInstant().toEpochMilli();
    }

    public String getPayrollReportDate(Long date) {
        LocalDate localDate = ofInstant(
                ofEpochMilli(date),
                of("UTC")
        );
        return localDate.format(outputFormatter);
    }

    public String formatToInput(Date future) {
        return ofInstant(
                future.toInstant(),
                of("UTC")
        ).format(inputFormatter);
    }
}
