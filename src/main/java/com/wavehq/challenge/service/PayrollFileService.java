package com.wavehq.challenge.service;

import com.wavehq.challenge.model.PayrollFile;
import com.wavehq.challenge.model.PayrollFileData;
import com.wavehq.challenge.repository.PayrollFileRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import static java.util.Objects.requireNonNull;

/**
 * The type Payroll file service.
 * I separated the business logic from the controller.
 * Moreover, I used FileStorageService to store the file in the disk separately, because it's not the main purpose of this file.
 * In addition, WorkingHoursHistoryService is used to store processed working hours for each employee.
 * Finally, I used the repository to store the data in the database.
 *
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class PayrollFileService {

    private final PayrollFileRepository payrollFileRepository;
    private final WorkingHoursHistoryService workingHoursHistoryService;
    private final FileStorageService fileStorageService;

    public void importFile(MultipartFile file) {
        final String id = requireNonNull(file.getOriginalFilename()).split(".csv")[0].split("-")[2];
        payrollFileRepository.findByFileId(id).ifPresentOrElse(payrollFile -> {
            log.info("File already exists, not allowed to import, skipping import");
            throw new IllegalStateException("File already exists, not allowed to import, skipping import");
        }, () -> {
            final PayrollFileData data = fileStorageService.storeFile(file);
            payrollFileRepository.save(PayrollFile.builder()
                    .fileId(id)
                    .filePath(data.getFileName())
                    .build()
            );
            data.getRecords().forEach(workingHoursHistoryLog -> {
                log.info("Saving record: {}", workingHoursHistoryLog);
                workingHoursHistoryService.saveRecordLog(workingHoursHistoryLog);
            });

        });
    }
}
