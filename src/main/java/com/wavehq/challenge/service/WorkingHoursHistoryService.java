package com.wavehq.challenge.service;

import com.wavehq.challenge.dto.EmployeeReport;
import com.wavehq.challenge.dto.PayPeriod;
import com.wavehq.challenge.dto.PayrollReport;
import com.wavehq.challenge.model.StartEndDate;
import com.wavehq.challenge.model.WorkingHoursHistory;
import com.wavehq.challenge.repository.WorkingHoursHistoryRepository;
import com.wavehq.challenge.repository.specification.WorkingHistoryBetween;
import com.wavehq.challenge.utils.DateUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Double.parseDouble;
import static java.lang.String.format;
import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toCollection;
import static org.springframework.data.jpa.domain.Specification.where;

/**
 * The type Working hours history service.
 * SaveRecordLog is a method that saves the record in the database.
 * GenerateReport is a method that generates the report.
 *
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class WorkingHoursHistoryService {

    private final WorkingHoursHistoryRepository workingHoursHistoryRepository;
    private final DateUtils dateUtils;

    public WorkingHoursHistory save(WorkingHoursHistory workingHoursHistory) {
        return workingHoursHistoryRepository.save(workingHoursHistory);
    }

    public WorkingHoursHistory saveRecordLog(String log) {
        final String[] tokens = log.split(",");
        final WorkingHoursHistory workingHoursHistory = WorkingHoursHistory.builder()
                .employeeId(tokens[2])
                .date(dateUtils.toEpoch(tokens[0]))
                .jobGroup(tokens[3])
                .workingHours(parseDouble(tokens[1]))
                .build();
        return save(workingHoursHistory);
    }

    public PayrollReport generateReport() {
        final Long minDate = workingHoursHistoryRepository.findFirstByOrderByDateAsc().getDate();
        final Long maxDate = workingHoursHistoryRepository.findFirstByOrderByDateDesc().getDate();
        StartEndDate startEndDate = dateUtils.getStartAndEndDate(minDate);

        List<EmployeeReport> reports = new ArrayList<>();
        while (startEndDate.getStartDate() <= maxDate) {

            final PayPeriod payPeriod = PayPeriod.builder()
                    .startDate(dateUtils.getPayrollReportDate(startEndDate.getStartDate()))
                    .endDate(dateUtils.getPayrollReportDate(startEndDate.getEndDate()))
                    .build();

            var spec = where(new WorkingHistoryBetween(startEndDate.getStartDate(), startEndDate.getEndDate()));
            workingHoursHistoryRepository.findAll(spec).stream()
                    .collect(groupingBy(WorkingHoursHistory::getEmployeeIdJobGroup)).forEach((k, v) -> {

                        final double sum = v.stream().mapToDouble(WorkingHoursHistory::getAmountPaid).sum();
                        reports.add(EmployeeReport.builder()
                                        .employeeId(k.employeeId())
                                        .payPeriod(payPeriod)
                                        .amountPaid("$" + format("%.2f", sum))
                                .build());

                    });

            final long nextDay = dateUtils.getNextDay(startEndDate.getEndDate());
            startEndDate = dateUtils.getStartAndEndDate(nextDay);

        }

        final ArrayList<EmployeeReport> sorted = reports.stream()
                .sorted(comparing(EmployeeReport::getEmployeeId).thenComparing(e -> e.getPayPeriod().getStartDate()))
                .collect(toCollection(ArrayList<EmployeeReport>::new));


        return PayrollReport.builder()
                .employeeReports(sorted)
                .build();

    }
}
