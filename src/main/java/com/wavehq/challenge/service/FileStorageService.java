package com.wavehq.challenge.service;

import com.wavehq.challenge.exception.FileStorageException;
import com.wavehq.challenge.model.PayrollFileData;
import com.wavehq.challenge.property.FileStorageProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;

import static java.nio.file.Files.*;
import static java.nio.file.Paths.get;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static java.util.Objects.requireNonNull;
import static java.util.Optional.of;
import static java.util.stream.Collectors.toCollection;
import static org.springframework.util.StringUtils.cleanPath;

@Slf4j
@Service
public class FileStorageService {

    private final Path fileStorageLocation;

    @Autowired
    public FileStorageService(FileStorageProperties fileStorageProperties) throws IOException {
        this.fileStorageLocation = get(fileStorageProperties.getUploadDir())
                .toAbsolutePath().normalize();

        final Path path = of(createDirectories(this.fileStorageLocation))
                .orElseThrow(() -> new FileStorageException("Could not create the directory where the uploaded files will be stored."));

        log.info("Upload directory is {}", path.toString());

    }

    public PayrollFileData storeFile(MultipartFile file) {

        String fileName = cleanPath(requireNonNull(file.getOriginalFilename()));

        return of(fileName)
                .filter(f -> !f.contains(".."))
                .map(f -> {
                    try {
                        Path targetLocation = fileStorageLocation.resolve(fileName);
                        copy(file.getInputStream(), targetLocation, REPLACE_EXISTING);
                        final ArrayList<String> records = lines(targetLocation).skip(1)
                                .collect(toCollection(ArrayList<String>::new));
                        return PayrollFileData.builder()
                                .fileName(targetLocation.getFileName().toString())
                                .records(records)
                                .build();
                    } catch (IOException e) {
                        log.error("Could not store file {}", fileName, e);
                    }
                    return null;
                })
                .orElseThrow(() -> new FileStorageException("Could not store file " + fileName + ". Please try again!"));

    }

}