package com.wavehq.challenge;

import com.wavehq.challenge.property.FileStorageProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import static java.lang.System.setProperty;
import static org.springframework.boot.SpringApplication.run;
import static org.springframework.core.env.AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME;


@Slf4j
@SpringBootApplication
@EnableConfigurationProperties(FileStorageProperties.class)
public class ChallengeSpringApplication {

    public static void main(String[] args) {
        log.info("Hello From Challenge");
        setProperty(ACTIVE_PROFILES_PROPERTY_NAME, "dev");
        run(ChallengeSpringApplication.class, args);
    }

}
