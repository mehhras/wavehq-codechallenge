drop table if exists payroll_file;
create table payroll_file (
    id varchar(36) primary key,
    file_id varchar(255) not null,
    file_path varchar(255) not null,
    uploaded_at bigint not null default (UNIX_TIMESTAMP())
);