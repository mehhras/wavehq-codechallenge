drop table if exists working_hours_history;
create table working_hours_history(
    id varchar(36) primary key,
    employee_id varchar(255) not null,
    date bigint not null,
    working_hours float not null,
    job_group varchar(255) not null
);

