package com.wavehq.challenge.unit.repository;

import com.wavehq.challenge.model.WorkingHoursHistory;
import com.wavehq.challenge.provider.WorkingHoursHistoryProvider;
import com.wavehq.challenge.repository.WorkingHoursHistoryRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
@DataJpaTest
class WorkingHoursHistoryRepositoryTest {

    @Autowired
    WorkingHoursHistoryRepository repository;

    WorkingHoursHistoryProvider workingHoursHistoryProvider;

    @BeforeEach
    void setup() {
        workingHoursHistoryProvider = new WorkingHoursHistoryProvider();
    }


    @RepeatedTest(3)
    void should_saveWorkingHistoryHours_when_OKModel() {
        final WorkingHoursHistory workingHoursHistory = workingHoursHistoryProvider.getWorkingHoursHistory();
        final WorkingHoursHistory save = repository.save(workingHoursHistory);

        assertThat(save).isNotNull();
        assertThat(save.getId()).isNotNull();
        assertThat(save.getEmployeeId()).isEqualTo(workingHoursHistory.getEmployeeId());
        assertThat(save.getDate()).isEqualTo(workingHoursHistory.getDate());
        assertThat(save.getWorkingHours()).isEqualTo(workingHoursHistory.getWorkingHours());
        assertThat(save.getJobGroup()).isEqualTo(workingHoursHistory.getJobGroup());
    }

    @RepeatedTest(3)
    void should_findWorkingHistoryHoursWithMinAndMaxDate_when_ListModels() {
        final List<WorkingHoursHistory> workingHoursHistories = workingHoursHistoryProvider.getWorkingHoursHistoryList();
        repository.saveAll(workingHoursHistories);

        final WorkingHoursHistory expectedMin = workingHoursHistories.get(0);
        final WorkingHoursHistory actualMin = repository.findFirstByOrderByDateAsc();

        assertThat(expectedMin.getEmployeeId()).isEqualTo(actualMin.getEmployeeId());
        assertThat(expectedMin.getDate()).isEqualTo(actualMin.getDate());
        assertThat(expectedMin.getWorkingHours()).isEqualTo(actualMin.getWorkingHours());
        assertThat(expectedMin.getJobGroup()).isEqualTo(actualMin.getJobGroup());


        final WorkingHoursHistory expectedMax = workingHoursHistories.get(workingHoursHistories.size() - 1);
        final WorkingHoursHistory actualMax = repository.findFirstByOrderByDateDesc();

        assertThat(expectedMax.getEmployeeId()).isEqualTo(actualMax.getEmployeeId());
        assertThat(expectedMax.getDate()).isEqualTo(actualMax.getDate());
        assertThat(expectedMax.getWorkingHours()).isEqualTo(actualMax.getWorkingHours());
        assertThat(expectedMax.getJobGroup()).isEqualTo(actualMax.getJobGroup());

    }

}
