package com.wavehq.challenge.unit.repository;

import com.wavehq.challenge.model.PayrollFile;
import com.wavehq.challenge.provider.PayrollFileProvider;
import com.wavehq.challenge.repository.PayrollFileRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
@DataJpaTest
class PayrollFileRepositoryTest {

    @Autowired
    PayrollFileRepository repository;

    PayrollFileProvider payrollFileProvider;

    @BeforeEach
    void setup() {
        payrollFileProvider = new PayrollFileProvider();
    }

    @RepeatedTest(3)
    void should_savePayrollFile_when_OKModel() {
        PayrollFile payrollFile = payrollFileProvider.getRandomPayrollFile();
        final PayrollFile save = repository.save(payrollFile);

        assertThat(save).isNotNull();
        assertThat(save.getId()).isNotNull();
        assertThat(save.getFileId()).isEqualTo(payrollFile.getFileId());
        assertThat(save.getFilePath()).isEqualTo(payrollFile.getFilePath());
    }

    @RepeatedTest(3)
    void should_findPayrollFile_when_ValidId() {
        PayrollFile payrollFile = payrollFileProvider.getRandomPayrollFile();
        final PayrollFile save = repository.save(payrollFile);

        final PayrollFile retrieved = repository.findByFileId(save.getFileId()).orElseThrow(() -> new RuntimeException("Not found"));

        assertThat(retrieved).isNotNull();
        assertThat(retrieved.getId()).isNotNull();
        assertThat(retrieved.getFileId()).isEqualTo(save.getFileId());
        assertThat(retrieved.getFilePath()).isEqualTo(save.getFilePath());

    }

}
