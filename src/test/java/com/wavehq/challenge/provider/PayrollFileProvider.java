package com.wavehq.challenge.provider;

import com.github.javafaker.Faker;
import com.wavehq.challenge.model.PayrollFile;

import java.util.Locale;

public class PayrollFileProvider {

    Faker enFaker;

    public PayrollFileProvider() {
        enFaker = new Faker(new Locale("en-US"));
    }

    public PayrollFile getRandomPayrollFile() {
        return PayrollFile.builder()
                .fileId(String.valueOf(enFaker.random().nextInt(100)))
                .filePath(enFaker.file().fileName())
                .build();
    }


}
