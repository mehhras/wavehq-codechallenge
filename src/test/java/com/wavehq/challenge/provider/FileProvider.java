package com.wavehq.challenge.provider;


import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;


public class FileProvider {

    public MultipartFile getFile() {
        return new MockMultipartFile("data", "time-report-42.csv", "text/plain",
                "date,hours worked,employee id,job group\n4/1/2023,10,1,A\n14/1/2023,5,1,A\n20/1/2023,3,2,B\n20/1/2023,4,1,A".getBytes());
    }

}
