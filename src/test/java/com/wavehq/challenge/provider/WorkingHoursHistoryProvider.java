package com.wavehq.challenge.provider;

import com.github.javafaker.Faker;
import com.wavehq.challenge.model.WorkingHoursHistory;
import com.wavehq.challenge.utils.DateUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static java.lang.String.valueOf;
import static java.util.Comparator.comparing;
import static java.util.concurrent.TimeUnit.DAYS;
import static java.util.stream.Collectors.toCollection;
import static java.util.stream.IntStream.range;

public class WorkingHoursHistoryProvider {

    Faker enFaker;
    DateUtils dateUtils;

    public WorkingHoursHistoryProvider() {
        dateUtils = new DateUtils();
        enFaker = new Faker(new Locale("en-US"));
    }

    public WorkingHoursHistory getWorkingHoursHistory() {
        return WorkingHoursHistory.builder()
                .employeeId(valueOf(enFaker.number().numberBetween(1, 100)))
                .workingHours(enFaker.number().randomDouble(1, 0, 24))
                .date(enFaker.date().future( enFaker.random().nextInt(1, 100), DAYS).getTime())
                .jobGroup(enFaker.lorem().characters())
                .build();
    }

    public List<WorkingHoursHistory> getWorkingHoursHistoryList() {
        return range(0, enFaker.number().numberBetween(1, 100))
                .mapToObj(i -> getWorkingHoursHistory())
                .sorted(comparing(WorkingHoursHistory::getDate))
                .collect(toCollection(ArrayList<WorkingHoursHistory>::new));
    }

    public String getRecordLog() {
        String date = dateUtils.formatToInput(enFaker.date().future( enFaker.random().nextInt(1, 100), DAYS));
        String employeeId = valueOf(enFaker.number().numberBetween(1, 100));
        String workingHours = valueOf(enFaker.number().randomDouble(1, 0, 24));
        String jobGroup = valueOf(enFaker.lorem().character()).toUpperCase();
        return date + "," + employeeId + "," + workingHours + "," + jobGroup;
    }

}
