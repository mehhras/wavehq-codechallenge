package com.wavehq.challenge.integration.service;

import com.wavehq.challenge.property.FileStorageProperties;
import com.wavehq.challenge.provider.FileProvider;
import com.wavehq.challenge.repository.PayrollFileRepository;
import com.wavehq.challenge.service.FileStorageService;
import com.wavehq.challenge.service.PayrollFileService;
import com.wavehq.challenge.service.WorkingHoursHistoryService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.Arrays;

import static java.util.Objects.requireNonNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.ANY;
import static org.springframework.test.annotation.DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD;

@Slf4j
@SpringBootTest
@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@DirtiesContext(classMode = BEFORE_EACH_TEST_METHOD)
@AutoConfigureTestDatabase(replace = ANY)
class PayrollFileServiceTest {

    PayrollFileService payrollFileService;

    @Autowired
    PayrollFileRepository payrollFileRepository;

    WorkingHoursHistoryService workingHoursHistoryService = mock(WorkingHoursHistoryService.class);

    @Autowired
    FileStorageService fileStorageService;

    FileProvider fileProvider;

    @Autowired
    FileStorageProperties fileStorageProperties;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);

        payrollFileService = new PayrollFileService(payrollFileRepository, workingHoursHistoryService, fileStorageService);
        fileProvider = new FileProvider();
    }

    @AfterEach
    public void tearDown() {
        File dir = new File(fileStorageProperties.getUploadDir());
        Arrays.asList(requireNonNull(dir.listFiles())).forEach(x -> {
            final boolean delete = x.delete();
            log.info("File {} deleted: {}", x.getName(), delete);
        });
    }

    @Test
    void should_importFile_when_NoExistingFile() {
        when(workingHoursHistoryService.saveRecordLog(any())).thenReturn(null);
        payrollFileService.importFile(fileProvider.getFile());

        verify(workingHoursHistoryService,times(4)).saveRecordLog(any());

    }

    @Test
    void should_throwException_when_ExistingFile() {
        final MultipartFile randomFile = fileProvider.getFile();

        // first import
        payrollFileService.importFile(randomFile);

        // second import should end to an exception
        assertThrows(IllegalStateException.class, () -> payrollFileService.importFile(randomFile));

    }

}
