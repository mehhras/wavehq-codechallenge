package com.wavehq.challenge.integration.service;

import com.wavehq.challenge.dto.EmployeeReport;
import com.wavehq.challenge.dto.PayPeriod;
import com.wavehq.challenge.dto.PayrollReport;
import com.wavehq.challenge.model.WorkingHoursHistory;
import com.wavehq.challenge.property.FileStorageProperties;
import com.wavehq.challenge.provider.FileProvider;
import com.wavehq.challenge.provider.WorkingHoursHistoryProvider;
import com.wavehq.challenge.repository.PayrollFileRepository;
import com.wavehq.challenge.repository.WorkingHoursHistoryRepository;
import com.wavehq.challenge.service.FileStorageService;
import com.wavehq.challenge.service.PayrollFileService;
import com.wavehq.challenge.service.WorkingHoursHistoryService;
import com.wavehq.challenge.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import static java.lang.Double.parseDouble;
import static java.util.Objects.requireNonNull;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
@SpringBootTest
@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
class WorkingHoursHistoryServiceTest {

    private WorkingHoursHistoryService workingHoursHistoryService;

    @Autowired
    private WorkingHoursHistoryRepository repository;

    private WorkingHoursHistoryProvider workingHoursHistoryProvider;
    private FileProvider fileProvider;

    DateUtils dateUtils;

    @Autowired
    PayrollFileRepository payrollFileRepository;
    PayrollFileService payrollFileService;

    @Autowired
    FileStorageProperties fileStorageProperties;

    FileStorageService fileStorageService;

    @BeforeEach
    public void setUp() throws IOException {
        MockitoAnnotations.openMocks(this);

        dateUtils = new DateUtils();
        workingHoursHistoryProvider = new WorkingHoursHistoryProvider();
        workingHoursHistoryService = new WorkingHoursHistoryService(repository, dateUtils);

        fileStorageService = new FileStorageService(fileStorageProperties);
        payrollFileService = new PayrollFileService(payrollFileRepository, workingHoursHistoryService, fileStorageService);

        fileProvider = new FileProvider();
    }

    @AfterEach
    public void tearDown() {
        File dir = new File(fileStorageProperties.getUploadDir());
        Arrays.asList(requireNonNull(dir.listFiles())).forEach(x -> {
            final boolean delete = x.delete();
            log.info("File {} deleted: {}", x.getName(), delete);
        });
    }

    @RepeatedTest(3)
    void should_saveRecordLog_when_validLog() {
        String log = workingHoursHistoryProvider.getRecordLog();
        final WorkingHoursHistory workingHoursHistory = workingHoursHistoryService.saveRecordLog(log);

        final String[] tokens = log.split(",");

        assertThat(workingHoursHistory.getEmployeeId()).isEqualTo(tokens[2]);
        assertThat(workingHoursHistory.getDate()).isEqualTo(dateUtils.toEpoch(tokens[0]));
        assertThat(workingHoursHistory.getWorkingHours()).isEqualTo(parseDouble(tokens[1]));
        assertThat(workingHoursHistory.getJobGroup()).isEqualTo(tokens[3]);

    }

    @Test
    void should_generateReportCorrectly_when_validDataset() {
        final MultipartFile file = fileProvider.getFile();
        payrollFileService.importFile(file);

        final PayrollReport payrollReport = workingHoursHistoryService.generateReport();

        assertThat(payrollReport.getEmployeeReports()).hasSize(3);

        final EmployeeReport employeeReport1 = payrollReport.getEmployeeReports().get(0);
        assertThat(employeeReport1.getEmployeeId()).isEqualTo("1");
        assertThat(employeeReport1.getAmountPaid()).isEqualTo("$300.00");
        assertThat(employeeReport1.getPayPeriod()).isEqualTo(PayPeriod.builder().startDate("2023-01-01").endDate("2023-01-15").build());

        final EmployeeReport employeeReport2 = payrollReport.getEmployeeReports().get(1);
        assertThat(employeeReport2.getEmployeeId()).isEqualTo("1");
        assertThat(employeeReport2.getAmountPaid()).isEqualTo("$80.00");
        assertThat(employeeReport2.getPayPeriod()).isEqualTo(PayPeriod.builder().startDate("2023-01-16").endDate("2023-01-31").build());

        final EmployeeReport employeeReport3 = payrollReport.getEmployeeReports().get(2);
        assertThat(employeeReport3.getEmployeeId()).isEqualTo("2");
        assertThat(employeeReport3.getAmountPaid()).isEqualTo("$90.00");
        assertThat(employeeReport3.getPayPeriod()).isEqualTo(PayPeriod.builder().startDate("2023-01-16").endDate("2023-01-31").build());

    }

}
