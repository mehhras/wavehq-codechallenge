package com.wavehq.challenge.integration.service;

import com.wavehq.challenge.model.PayrollFileData;
import com.wavehq.challenge.property.FileStorageProperties;
import com.wavehq.challenge.provider.FileProvider;
import com.wavehq.challenge.service.FileStorageService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import static java.util.Objects.requireNonNull;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
@SpringBootTest
@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@EnableConfigurationProperties(value = FileStorageProperties.class)
class FileStorageServiceTest {

    FileStorageService fileStorageService;

    @Autowired
    FileStorageProperties fileStorageProperties;

    FileProvider fileProvider;


    @BeforeEach
    public void setUp() throws IOException {
        fileStorageService = new FileStorageService(fileStorageProperties);
        fileProvider = new FileProvider();
    }

    @AfterEach
    public void tearDown() {
        File dir = new File(fileStorageProperties.getUploadDir());
        Arrays.asList(requireNonNull(dir.listFiles())).forEach(x -> {
            final boolean delete = x.delete();
            log.info("File {} deleted: {}", x.getName(), delete);
        });
    }

    @RepeatedTest(3)
    void should_saveFile_when_fileIsOK() {
        final PayrollFileData payrollFileData = fileStorageService.storeFile(fileProvider.getFile());

        assertThat(payrollFileData).isNotNull();
        assertThat(payrollFileData.getFileName()).isNotNull();
        assertThat(payrollFileData.getFileName()).isNotEmpty();
        assertThat(payrollFileData.getFileName()).isNotBlank();
        assertThat(payrollFileData.getRecords()).isNotNull();
        assertThat(payrollFileData.getRecords()).isNotEmpty();
        assertThat(payrollFileData.getRecords()).hasSizeGreaterThan(0);

    }


}
