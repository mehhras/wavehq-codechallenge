<h1> Documentation: </h1>


<h3>Prerequisites:</h3>
1. Mysql or Any other sql database <br>
2. Java 15 or above <br>
3. Git <br>


<h3> Instructions on how to build/run the application: </h3> <br>

1. Clone the repository:
```
git clone https://gitlab.com/mehhras/wavehq-codechallenge.git
```
2. Change directory to src/main/resources. Rename application-dev.yml.example to application-dev.yml and edit keys with correct values based on your database variables.<br>
3. Change directory to src/test/resources. Rename application-test.yml.example to application-test.yml and edit keys with correct values. <br>

4. Change directory to wavehq-codechallenge. Build the application:
```
gradlew build
```

5. Run the application:
```
gradlew bootRun
```

<h2>Questions:</h2>
<h6>Q) How did you test that your implementation was correct?</h6>
A) I used junit5 and mockito as test frameworks and wrote some tests for the application. Moreover, I tested the application by other users.

<h6>Q) If this application was destined for a production environment, what would you add or change? </h6>
A) First, I definitely would add docker and CI/CD support to the application. <br>
Secondly, I would add a security layer to the application. <br>
Thirdly, I would add more tests for the application.

<h6>Q) What compromises did you have to make as a result of the time constraints of this challenge? </h6>
A) As documentation said, I had to consider some comprises such as csv files, and its formatted content <br>
I had to use less functional programming techniques.
